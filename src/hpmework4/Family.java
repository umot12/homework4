package hpmework4;

import java.util.stream.IntStream;


public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;


    public Family(Human mother,Human father){
             this.mother = mother;
             this.father = father;
    }

    public Human getMother() {
        return mother;
    }
    public int getCount() {
        int count = 2 + children.length;
        return count;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }
    public void addChild(Human[] child) {
        children = child;
    }
    public void deleteChild(int index) {
        children = remove(children, index);
    }

    private static Human[] remove(Human[] a, int index)
    {
        if (a == null || index < 0 || index >= a.length) {
            return a;
        }

        Human[] result = new Human[a.length - 1];
        for (int i = 0; i < index; i++) {
            result[i] = a[i];
        }

        for (int i = index; i < a.length - 1; i++) {
            result[i] = a[i + 1];
        }

        return result;
    }

    public Human[] getChildren() {
        return children;
    }
    public String returnnStringChildren(Human[] arary){
        String string="";

        for(int i = 0;i  < arary.length;i++){
            if(!string.equals(""))string+=", ";
            string+= "["+arary[i].getName() + ',' + arary[i].getSurname()+ ',' + arary[i].getYear()+"]";
        }
        string = "["+string+"]";
        return string;
    }

    public void setChildren(String[] children) {
        //this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        String Sms = "Family{"
                + "mother='" + getMother()
                + "\n father='" + getFather()
                + "\n children='" + returnnStringChildren(getChildren())
                + "\n pet='" + getPet()
                + "\n count =" + getCount()
                +"}";

        return Sms;
    }
}
