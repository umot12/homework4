package hpmework4;

import javax.swing.*;
import java.util.Arrays;

public class Human {

    private Family family;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet ;

    private String [][] schedule;

    public Human(String name,String surname,int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name,String surname,int year,int iq,Pet pet,String [][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void greetPet(String name){
        System.out.println("hi " + name);
    }
    public void  greetPet(String species,int age,int trickLevel){
        System.out.println("У меня есть" + species +", ему"+ age + "лет, он"+ (trickLevel > 50? "он очень хитрий":"он не очень хитрый "));

    }
    public String returnnStringSchedule(String[][] arary){
        String string="";

        for(int i = 0;i  < arary.length;i++){
            if(!string.equals(""))string+=", ";
            string+= "["+arary[i][0] + ',' + arary[i][1]+"]";
        }
        string = "["+string+"]";
        return string;
    }
    //у класса Human должен выводить сообщение следующего вида:
  //  Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone, father=Vito Karleone, pet=dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}
    @Override
    public String toString() {
        String Sms = "Human{name='"+getName()+"," +
                "surname=" + getSurname()+"," +
                "year=" + getYear() +"," +
                "iq=" + getIq()+"," +
                "schedule=" + returnnStringSchedule(getSchedule()) +
                "}";
        return Sms;
    }



}


